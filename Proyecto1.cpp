#include "mbed.h"
#include "Servo.h"    //Librerias

Servo Pulgar(PA_8);   //Servos
Servo Indice(PA_9);
Servo Medio(PA_10);
Servo AnMe(PC_7);
Servo Codo(PA_3); //codo bajar/subir
Servo Mu(PA_2);
Servo Antebrazo(PB_4);
Servo Codo2(PB_3);//codo rotar

float c, cc, a, m, d,x;
int y, opcio;

void mover_codo(float x)
{
    if ((50<=x) and (x<=90)){//grados aproximados
		Codo.position(x);
        }
	else{
		printf("Grau no valid. Tria un entre 50 i 90.\n");
        }
}

void rotar_codo2(float x)
{
    if ((0<=x) and (x<=180)){
		Codo2.position(x);
        }
	else{
		printf("Grau no valid. Tria un entre 0 i 180.\n");
        }
}

void mover_antebrazo(float x)
{
    if ((0<=x) and (x<=90)){
		Antebrazo.position(x);
        }
	else{
		printf("Grau no valid. Tria un entre 0 i 90.\n");
        }
}

void mover_mu(float x)
{
    if ((0<=x) and (x<=90)){
		Mu.position(x);
        }
	else{
		printf("Grau no valid. Tria un entre 0 i 90.\n");
        }
}

void mover_dedos(float x)
{
    if ((0<=x) and (x<=180)){ //se deberia de mirar al tener todo montado
		Pulgar.position(x);
		Indice.position(x);
		Medio.position(x);
		AnMe.position(x);}
	else{
		printf("Grau no valid. Tria un entre 0 i 180 . \n");
        }
}



int main() {
    Pulgar.position(0);
//Pulgar.position(0); (FORMA1: con grados)
//Pulgar.position(180); 
//Pulgar.position(-180);
//Pulgar=0.5 (valor entre 0 y 1) (FORMA2: en tanto por 1)  
    Indice.position(0);
    Medio.position(0);
    AnMe.position(0);
    Codo2.position(0);
    Codo.position(0);
    Mu.position(0);
    Antebrazo.position(0);

    while (y==1)
    {
    	printf("1. Moure el colze. \n" );
    	printf("2. Rotar el colze. \n" );
    	printf("3. Moure l'avantbraç. \n" );
    	printf("4. Rotar el canell. \n" );
		printf("5. Moure els dits. \n" );
		printf("Tria una opció: \n");
		scanf("%d", &opcio);
    	
    	if (opcio == 1)
        {
    		printf("Tria quin grau vols moure el colze:\n");
    		scanf("%f",&c);
    		mover_codo(c);
        }

    	if (opcio==2)
        {
    		printf("Tria quin grau vols rotar el colze:\n");
    		scanf("%f",&cc);
    		rotar_codo2(cc);
        }

    	if (opcio == 3)
        {
    		printf("Tria quin grau vols moure l'avantbraç:\n");
    		scanf("%f",&a);
    		mover_antebrazo(a);
        }

    	if (opcio==4)
        {
    		printf("Tria quin grau vols rotar el canell:\n");
    		scanf("%f",&m);
    		mover_mu(m);
        }

    	if (opcio==5)
        {
    		printf("Tria quin grau vols moure els dits:\n");
    		scanf("%f",&d);
    		mover_dedos(d);
        }
    }
}